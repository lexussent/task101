Readme -

Table of contents


About the project

Technologies

Getting started

Usage

**About the project**

This project is to create and provision an AWS infrastructure using Terraform. The client has an application which is currently hosted on premises and wants to deploy it to the cloud for the many benefits the cloud offers. The services will be hosted in docker containers and the front end will be accessible via the elastic IP.


**Technologies**

Docker
Python
AWS
Terraform

**Getting started**

_Prerequisites_ 

AWS Vault credentials setup for secure logins.

Terraform 
“brew install hashicorp/tap/terraform”

Git
“brew install git”

AWS vault
“brew install --cask aws-vault”

Usage

_Cloning the repo_


Open terminal then make the task101 folder in the following directory.
Mkdir /etc/task101

Navigate then clone the repo into the newly created directory
“ Git clone https://gitlab.com/lexussent/task101.git”

_Updating variables_

In the terraform.tfvars folder, change the variables to match your use case. It’s important that the private key is in the correct location, or you will not be able to ssh to the host.

_Applying terraform config_

Log into aws using the aws-vault exec “name of environment” on the terminal.

Run the “terraform apply”, then confirm the changes. (You need to be in the tasks101 folder when running terraform commands)

The AWS environment should be setup, along with the EC2, database and docker containers.

_Interacting with the application_

Navigate to the public IP address AWS allocated the host on port 8080. A page asking for the names of the users checking in and out should appear, enter in the credentials to send the data to the database. You can check that the data has successfully been sent to the back end by viewing the database on AWS.
