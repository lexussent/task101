# Variables to be used throughout the config. 
variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
variable allowed_ips {}
variable instance{}
variable private_key {}
variable image_name {}