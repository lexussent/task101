# Configure the AWS Provider and the zone
provider "aws" {
  region = "eu-north-1"
}


module "subnet" {
    source = "./modules/subnet"
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
    vpc_id = aws_vpc.prod-vpc.id
}

module "webserver" {
    source = "./modules/webserver"
    vpc_id = aws_vpc.prod-vpc.id
    allowed_ips = var.allowed_ips
    env_prefix = var.env_prefix
    image_name = var.image_name
    private_key = var.private_key
    instance = var.instance
    subnet_id = module.subnet.subnet.id
    avail_zone = var.avail_zone
}

# Create a VPC for the EC2 to reside in
resource "aws_vpc" "prod-vpc" {
  cidr_block = var.vpc_cidr_block
  instance_tenancy = "default"
  tags = {
    Name: "${var.env_prefix}-vpc"
  }
}

/* #Create the Database used for the backend, follows the scheme of the dyanmodb file
resource "aws_dynamodb_table" "Database" {
  name           = "UserCheckin"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "Username"

  attribute {
    name = "Username"
    type = "S"
  }
} */

/* # Create iam role and profile used for the boto
resource "aws_iam_instance_profile" "lexus_instance_profile" {
    name = "lexus_instance_profile"
    role = aws_iam_role.lexus_role.name
}

resource "aws_iam_role" "lexus_role" {
    name = "lexus_role"
    assume_role_policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
            { 
                Action = "sts:AssumeRole"
                Effect = "Allow"
                Sid    = ""
                Principal = {
                    Service = "ec2.amazonaws.com"
                }
            }
        ]
    })
} */


/* /* resource "aws_iam_policy_attachment" "lexus_role_attachment" {
    name = "lexus_role_attachment"
    roles = [ aws_iam_role.lexus_role.name ]
    policy_arn = aws_iam_policy.allow_dynamodb.arn

# Set allowed actions for the database which is associated with the policy
resource "aws_iam_policy" "allow_dynamodb" {
    name = "allow_dynamodb"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.Database.arn}"
    }
  ]
}
EOF
} */