# Security group - Granual rules to allow on specified IPs to reach the host.

resource "aws_security_group" "prod-sg" {
    name = "prod-sg"
    vpc_id = var.vpc_id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = var.allowed_ips
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = var.allowed_ips
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }

    tags = {
        Name: "${var.env_prefix}-sg"
    }
}
# Query AWS for latest Amazon image

data "aws_ami" "latest-aws-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = [var.image_name]
    }
}


# Create EC2 instance Linux Server for hosting microservices, uses custom VPC and subnet
# Use terraform to provision commands to setup docker containers to host the services
# Containers are within the same docker network so they are able to communicate with each other
# Send files to the EC2 instance
# Encrypt the hard disk of the device
# Static IPs set in the docker command so I could hardcode the destination IP for the backend in the front end's code
resource "aws_instance" "prod-server" {
    ami = data.aws_ami.latest-aws-linux-image.id
    instance_type = var.instance
    subnet_id = var.subnet_id
    vpc_security_group_ids = [aws_security_group.prod-sg.id]
#    iam_instance_profile = aws_iam_instance_profile.lexus_instance_profile.name
    availability_zone = var.avail_zone
    associate_public_ip_address = true
    key_name = "testing"
    connection {
        type = "ssh"
        host = self.public_ip
        user = "ec2-user"
        private_key = file(var.private_key)
    }
    provisioner "file" {
        source = "/etc/git/devops/task101/app"
        destination = "/home/ec2-user/app"
    }
    provisioner "remote-exec" {
        inline = [
            "sudo yum update -y && sudo yum install -y docker",
            "sudo usermod -aG docker ec2-user",
            "sudo systemctl start docker",
            "sudo docker build app/frontend -t frontend-prod",
            "sudo docker build app/backend -t backend-prod",
            "sudo docker network create production --subnet=172.20.0.0/24",
            "sudo docker run --network=production --ip 172.20.0.2 -d -p 8080:8080 frontend-prod",
            "sudo docker run --network=production --ip 172.20.0.3 -d backend-prod",

        ]
    }
    root_block_device {
        encrypted = true
    }
    tags = {
        Name = "${var.env_prefix}-server"
    }
}



