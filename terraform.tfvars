vpc_cidr_block = "172.16.0.0/16"
subnet_cidr_block = "172.16.1.0/24"
avail_zone = "eu-north-1a"
env_prefix = "prod"
allowed_ips = ["82.32.170.143/32"]
instance = "t3.small"
private_key = "/Users/user2238/.ssh/testing.pem"
image_name = "amzn2-ami-hvm-*-x86_64-gp2"